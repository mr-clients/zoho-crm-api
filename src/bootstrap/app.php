<?php 

require_once __DIR__ . '/../vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Formatter\JsonFormatter;
use Respect\Validation\Validator as v;

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => $_SERVER['DISPLAY_ERROR_DETAILS'],
        'logfile' => __DIR__ . '/../logs/api.log',
    ]
]);

$container = $app->getContainer();

$container['logger'] = function ($container) {
    // var_dump($container['settings']['logfile']);exit;
    $logger = new \Monolog\Logger( 'api_log' );
    $stream_handler = new \Monolog\Handler\StreamHandler( $container['settings']['logfile'], Logger::INFO );
    $stream_handler->setFormatter( new \Monolog\Formatter\JsonFormatter() );
    $logger->pushHandler( $stream_handler );
    return $logger;
};

$container['accounts'] = function ($container) {
    return new \App\Models\Accounts(__DIR__ . '/../' . $container['settings']['accountsPath']);
};

// add Auth class
$container['auth'] = function ($container) {
    return new \App\Auth\Auth($container);
};

// add views to the application
$container['view'] = function ($container) {

    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
        'cache' => false,
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

	// calculate css version by last changed
    $base_css_path = pathinfo(__DIR__ . '/../css/app.css');
    $base_css_version = filemtime($_SERVER['DOCUMENT_ROOT'].$url);
    $view->getEnvironment()->addGlobal('base_css_version', $base_css_version);

	// let the view have access to auth controller
    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container->auth->check(),
        'user' => $container->auth->userAsArray(),
    ]);
    
    // let the view have access to flash messages
    $view->getEnvironment()->addGlobal('flash', $container->flash);

    return $view;

};

$container['validator'] = function ($container) {
    return new App\Validation\Validator;
};

$container['HomeController'] = function ($container) {
    return new \App\Controllers\HomeController($container);
};

$container['AuthController'] = function ($container) {
    return new \App\Controllers\Auth\AuthController($container);
};

$container['PasswordController'] = function ($container) {
    return new \App\Controllers\Auth\PasswordController($container);
};

$container['TokenController'] = function ($container) {
    return new \App\Controllers\Auth\TokenController($container);
};

$container['UsersController'] = function ($container) {
    return new \App\Controllers\UsersController($container);
};

$container['LeadController'] = function ($container) {
    return new \App\Controllers\LeadController($container);
};

$container['ApiController'] = function ($container) {
    return new \App\Controllers\ApiController($container);
};

$container['AccountController'] = function ($container) {
    return new \App\Controllers\AccountController($container);
};

// add Slim Flash messages
$container['flash'] = function ($container) {
    return new \Slim\Flash\Messages;
};

// add Slim CSRF
$container['csrf'] = function ($container) {
    return new \Slim\Csrf\Guard;
};

// give back errors
$app->add(new App\Middleware\ValidationErrorsMiddleware($container));
// give back the old input
$app->add(new App\Middleware\OldInputsMiddleware($container));
// give back a csrf generated key
$app->add(new App\Middleware\CsrfViewMiddleware($container));

// run the crsf check
// $app->add($container->csrf);

// setup custom rules
v::with('App\\Validation\\Rules\\');

require_once __DIR__ . '/../app/routes.php';
