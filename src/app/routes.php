<?php 

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;
use App\Middleware\ApiMiddleware;

$app->get('/', 'HomeController:index')->setName('home');

$app->group('', function(){
    $this->get('/auth/login', 'AuthController:getLogin')->setName('auth.login');

    $this->post('/auth/login', 'AuthController:postLogin');
})->add(new GuestMiddleware($container));

$app->group('', function(){
    $this->get('/auth/password/change[/{id}]', 'PasswordController:getChangePassword')->setName('auth.password.change');

    $this->post('/auth/password/change', 'PasswordController:postChangePassword');

    $this->get('/api/zoho/view', 'TokenController:getToken')->setName('api.zoho.view');

    $this->post('/api/zoho/regenerate[/{id}]', 'TokenController:postTokenRegenerate')->setName('api.zoho.regenerate');

    $this->get('/auth/logout', 'AuthController:logout')->setName('auth.logout');

    $this->get('/accounts', 'AccountController:getAccounts')->setName('accounts');

    $this->get('/accounts/edit[/{id}]', 'AccountController:getAccount')->setName('accounts.edit');

    $this->post('/accounts/edit', 'AccountController:postAccount');

    $this->get('/accounts/create', 'AccountController:getCreate')->setName('accounts.create');

    $this->post('/accounts/create', 'AccountController:postCreate');

    $this->get('/api/zoho/setup-credentials', 'ApiController:getZohoCredentials')->setName('api.zoho.setup-credentials');

    $this->post('/api/zoho/setup-credentials', 'ApiController:postZohoCredentials');

    $this->get('/api/zoho/self-client', 'ApiController:getSelfClient')->setName('api.zoho.self-client');

    $this->post('/api/zoho/self-client', 'ApiController:postSelfClient');

    $this->get('/api/logs', 'ApiController:getLogging')->setName('api.logs');

})->add(new AuthMiddleware($container));

$app->group('/v2', function(){
    $this->get('/leads',  'LeadController:getLeads');
    $this->post('/leads',  'LeadController:addLead');
    $this->post('/token', 'ApiController:addToken');
    $this->map(['GET', 'POST'],'/homeadvisor/feed',  'LeadController:addLead');
})->add(new ApiMiddleware($container));
