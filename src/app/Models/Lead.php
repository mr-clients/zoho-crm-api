<?php
/*
 * Lead
 */
namespace App\Models;

/*
 * Lead
 */
class Lead {
    /* @var UUID $consumer Unique ID of the lead generation API consumer pushing leads. */
    private $consumer;
/* @var string $apiKey API Key provided to the lead generation API consumer. */
    private $apiKey;
/* @var string $firstName  */
    private $firstName;
/* @var string $lastName  */
    private $lastName;
/* @var string $address1  */
    private $address1;
/* @var string $address2  */
    private $address2;
/* @var string $city  */
    private $city;
/* @var string $state  */
    private $state;
/* @var string $postalCode  */
    private $postalCode;
/* @var string $phonePrimary  */
    private $phonePrimary;
/* @var string $email  */
    private $email;
/* @var string $comments  */
    private $comments;
/* @var Number $taskId  */
    private $taskId;
/* @var string $taskName  */
    private $taskName;
/* @var string $interview  */
    private $interview;
/* @var string $matchType  */
    private $matchType;
/* @var string $leadDescription  */
    private $leadDescription;
}
