<?php 

namespace App\Models;

class Accounts {

    protected $accounts_path;

    public function __construct($accounts_path = __DIR__ . '/../../accounts') 
    {
        $this->accounts_path = $accounts_path;    
    }

    protected function getAccountFile($username) 
    {
        return join(DIRECTORY_SEPARATOR, [$this->accounts_path, $username . '.yaml']);
    }

    public function getAccountExists($username)
    {
        $account_file_path = self::getAccountFile($username);
        return file_exists($account_file_path);
    }

    public function getAccount($username)
    {
        if ( self::getAccountExists($username) ) {
            return yaml_parse_file( self::getAccountFile($username), 0 );
        }
        return false;
    }

    public function getAccounts()
    {
        $account_files = glob($this->accounts_path . '/*.yaml');

        $accounts = [];
        foreach ($account_files as $account_file) {
            $accounts[] = yaml_parse_file( $account_file, 0 );
        }
        return $accounts;
    }

    public function saveFile($username, $account)
    {
        $account_file_path = self::getAccountFile($username);
        return yaml_emit_file($account_file_path, $account);
    }
}