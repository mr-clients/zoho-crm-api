<?php 

namespace App\Models;

use App\Models\Accounts;

class User {

    protected $username; 
    protected $account;

    public function __construct($username) 
    {
        $this->username = $username;
        $accounts = new Accounts();
        $this->account = $accounts->getAccount($username);  
        return $this;  
    }

    public function adminCanLogin()
    {
        
        if ($this->account) {
            if ($this->state == 'enabled') {
                if (isset($this->account['access']['admin']['login']) && $this->account['access']['admin']['login'])
                {
                    return true;
                }
            }
        }

        return false;
    }

    public function verifyPassword($password)
    {
        return password_verify($password, $this->hashed_password);
    }

    public function setPassword($password) 
    {
        $this->account['hashed_password'] = password_hash($password, PASSWORD_DEFAULT);
        $accounts = new Accounts();
        return $accounts->saveFile($this->username, $this->account);
    }

    public function generateToken()
    {
        $token = bin2hex(openssl_random_pseudo_bytes(12));
        $this->account['hashed_apikey'] = password_hash($token, PASSWORD_DEFAULT);
        $accounts = new Accounts();
        if( $accounts->saveFile($this->username, $this->account) ) {
            return $token;
        } else {
            return false;
        }
    }

    public function updateAccount($request, $create = false)
    {
        $this->account['state'] = ($create)  ? 'enabled' : $request->getParam('state');
        $this->account['fullname'] = $request->getParam('fullname');
        $this->account['email'] = $request->getParam('email');
        $request_access = $request->getParam('access');
        $access = [];
        foreach ($request_access as $request_privileges) {
            $request_privileges = explode('.', $request_privileges);

            switch (count($request_privileges)) {
                case 2:
                    $access[$request_privileges[0]][$request_privileges[1]] = true;
                    break;
                case 3:
                    $access[$request_privileges[0]][$request_privileges[1]][$request_privileges[2]] = true;
                    break;
                case '4':
                    $access[$request_privileges[0]][$request_privileges[1]][$request_privileges[2]][$request_privileges[3]] = true;
                    break;
                default:
                    var_dump(count($request_privileges));exit;
            }
        }
        $this->account['access'] = $access;

        if ($create) {
            $this->account['id'] = $request->getParam('id');
            $this->account['hashed_password'] = password_hash($request->getParam('password'), PASSWORD_DEFAULT);
            $this->account['hashed_apikey'] = password_hash($request->getParam('token'), PASSWORD_DEFAULT);
        }
        $accounts = new Accounts();
        return $accounts->saveFile($this->username, $this->account);
    }

    public function exists()
    {
        return ($this->account === false) ? false : true;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function __get($property)
    {
        return $this->account[$property];
    }
    
}