<?php 

namespace App\Models;

use zcrmsdk\crm\setup\restclient\ZCRMRestClient;
use zcrmsdk\crm\crud\ZCRMRecord;
use zcrmsdk\oauth\ZohoOAuth;
use zcrmsdk\crm\crud\ZCRMModule;
use zcrmsdk\oauth\exception\ZohoOAuthException;

class Zoho 
{

    protected $settings_path, $configuration, $request_hash;
    public function __construct() 
    {
        $this->settings_path = __DIR__ . '/../../config/zoho-settings.yaml';
        $this->configuration = yaml_parse_file($this->settings_path, 0);
        foreach ([
            'client_id' => 'CLIENT_ID', 
            'client_secret' => 'CLIENT_SECRET', 
            'redirect_uri' => 'REDIRECT_URI', 
            'currentUserEmail' => 'CURRENT_USER_EMAIL', 
            'sandbox' => 'SANDBOX'
        ] as $key => $value) {
            if (! isset($this->configuration[$key])) {
                $this->configuration[$key] = $_SERVER[$value];
            }
        }
        try {
            ZCRMRestClient::initialize($this->configuration);
        } catch (ZohoOAuthException $e) {
            return false;
        }
    }

    // generate new access token from self client token
    public function addToken($request)
    {
        try {
            $oAuthClient = ZohoOAuth::getClientInstance();
            $oAuthTokens = $oAuthClient->generateAccessToken($request->getParam('token'));
            return true;
        } catch (ZohoOAuthException $e) {
            return false;
        }
    }

    // pull leads from the CRM and return
    public function getLeads()
    {
        
        $zcrmModuleIns = ZCRMModule::getInstance("Leads");
        $bulkAPIResponse = $zcrmModuleIns->getRecords();
        $recordsArray = $bulkAPIResponse->getData();
        return $recordsArray;
    }

    // add lead to CRM and return response
    public function addLead($request)
    {
        // setup record
        $moduleIns = ZCRMRestClient::getInstance()->getModuleInstance("Leads"); //to get the instance of the module
        $record = ZCRMRecord::getInstance("Leads",null);  //To get ZCRMRecord instance

        // set fields 
        $record->setFieldValue("First_Name", $request->getParam('firstName')); 
        $record->setFieldValue("Last_Name", $request->getParam('lastName')); 
        $record->setFieldValue("Street", $request->getParam('address')); 
        $record->setFieldValue("City", $request->getParam('city')); 
        $record->setFieldValue("State", $request->getParam('stateProvince')); 
        $record->setFieldValue("Zip_Code", $request->getParam('postalCode')); 
        $record->setFieldValue("Phone", $request->getParam('primaryPhone')); 
        $record->setFieldValue("Ext", $request->getParam('phoneExt')); 
        $record->setFieldValue("Mobile", $request->getParam('secondaryPhone')); 
        $record->setFieldValue("Ext_2", $request->getParam('secondaryPhoneExt')); 
        $record->setFieldValue("Email", $request->getParam('email')); 
        $record->setFieldValue("HA_Lead_Cost", str_replace('$', '', $request->getParam('fee'))); 
        $record->setFieldValue("Lead_Notes", $request->getParam('comments')); 

        $appointment = $request->getParam('appointment');
        $record->setFieldValue("Lead_Status", $appointment['status']); 
        $record->setFieldValue("Primary_Lead_Type", $appointment['type']); 
        // 2015-06-09T12:00:00.000-07:00
        preg_match('/(\d{4}-\d{2}-\d{2})(T\d{2}:\d{2}:\d{2}).*(-\d{2}:\d{2})/', $appointment['start'], $matches);
        $record->setFieldValue("Appointment_Date", $matches[1]);// . $matches[2]); 

        $record->setFieldValue("All_Form_Info", "Task Name: " . $request->getParam('taskName') . "
Appointment ID: " . $appointment['appointmentOid'] . "
Appointment status: " . $appointment['status'] . "
Appointment type: " . $appointment['type'] . "
Appointment start: " . $appointment['start'] . "
Appointment end: " . $appointment['end'] . "
Appointment external_id: " . $appointment['external_id']); 

// Description
        $description = '';
        foreach ($request->getParam('interview') as $interview) {
            $description .= "Question: " . $interview['question'] . "
Answer: " . $interview['answer'] . "
";
        }
        $record->setFieldValue("Description", $description); 

        // create record & return response as JSON
        $responseIn = $moduleIns->createRecords([$record]);
        $responses = $responseIn->getEntityResponses();
        return $responses[0]->getResponseJSON();
    }

    public function saveCredentials($request)
    {
        $this->configuration['client_id'] = $request->getParam('client_id');
        $this->configuration['client_secret'] = $request->getParam('client_secret');
        $this->configuration['redirect_uri'] = $request->getParam('redirect_uri');
        $this->configuration['currentUserEmail'] = $request->getParam('current_user_email');

        return yaml_emit_file($this->settings_path, $this->configuration);
    }
}