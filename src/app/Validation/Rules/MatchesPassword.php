<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

class MatchesPassword extends AbstractRule
{

    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function validate($input)
    {
        return $this->user->verifyPassword($input);
    }
}