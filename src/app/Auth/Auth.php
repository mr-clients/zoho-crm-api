<?php

namespace App\Auth;

use App\Models\User;
use Respect\Validation\Validator as v;

class Auth 
{
    protected $container;

    /**
     * Set container on class
     *
     * @param   Object  $container  Container object
     *
     * @return  void              
     */
    public function __construct($container) 
    {
        $this->container = $container;
    }

    /**
     * Gets user object 
     *
     * @return  Object  User class object
     */
    public function user()
    {
        // return false if session user is not set
        if ( !isset($_SESSION['user']) ) return false;
        return new User($_SESSION['user']);
    }

    /**
     * Return user as an array
     *
     * @return  array  User account array
     */
    public function userAsArray()
    {
        // return false if session user is not set
        if ( !isset($_SESSION['user']) ) return false;
        $User = new User($_SESSION['user']);
        return $User->getAccount();
    }

    /**
     * Check if user is signed in
     *
     * @return  bool  Boolean of whether user sesson is set
     */
    public function check() 
    {
        return isset($_SESSION['user']);
    }

    /**
     * Check api consumers ability to consume api route & method
     *
     * @param   Object  $request  Slim request object
     *
     * @return  bool            Whether or not the user can consume API
     */
    public function apiCheck($request)
    {
        // setup validation for consumer & apikey fields
        $validation = $this->container->validator->validate($request, [
            'consumer' => v::notEmpty()->intVal(),
            'apikey' => v::alnum()->length(8, null)->notEmpty(),
        ]);

        // check if validation failed
        if ( $validation->failed()) {
            return false;
        }

        // get account information based on consumer id
        $consumer = $request->getParam('consumer');
        $User = new User($consumer);
        $account = $User->getAccount();

        // check if the apiKey that is passed matched stored key hash
        if ( ! password_verify($request->getParam('apikey'), $account['hashed_apikey']) ) {
            return false;
        }

        // check that an account was found
        if ($account) {
            // determine api privilege needed by request method
            $method = ($request->getMethod() == "GET") ? 'read' : 'write';
            // explode path to iterate through permissions levels and remove initial slash & first element
            $paths = explode( '/', ltrim( $request->getUri()->getPath(), '/' ) );
            // pull access array
            $access = $account['access']['api'];
            // loop through access privileges by moving down the path elements
            foreach ($paths as $path) {
                if ( in_array($path, ["v2", "sb"]) ) {
                    continue;
                }
                // shorten array to current path element
                $access = $access[$path];
            }

            // check whether the method is defined and whether the user has permission
            if (isset($access[$method]) && $access[$method]) {
                return true;
            }
        }
        return false; 
    }

    /**
     * Attempt login
     *
     * @param   string  $username  username
     * @param   string  $password  user password
     *
     * @return  bool             whether user can login
     */
    public function attempt($username, $password) 
    {
        // Get User object
        $User = new User($username);
        $account = $User->getAccount();
        // If account is false then
        if ($account === false) {
            return false;
        }

        
        if ($User->adminCanLogin()) {
            
            if ($User->verifyPassword($password)) {
                $_SESSION['user'] = $username;
                return true;
            }
        }
    }

    public function logout() {
        unset($_SESSION['user']);
    }
}