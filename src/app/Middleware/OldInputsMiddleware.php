<?php 

namespace App\Middleware;

class OldInputsMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {

        $this->container->view->getEnvironment()->addGlobal('oldinputs', $_SESSION['oldinputs']);
        $_SESSION['oldinputs'] = $request->getParams();

        $response = $next($request, $response);

        return $response;
    }
}