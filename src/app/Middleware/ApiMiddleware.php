<?php

namespace App\Middleware;

class ApiMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{
		// check if the user is signed in
		if (!$this->container->auth->apiCheck($request)) {
			$this->container->logger->error('API.authentication.error', [
				'hash' => hash('md5', implode(',', $request->getParams())),
				'method' => $request->getMethod(),
				'request' => $request->getParams(),
				'body' => $request->getBody()
			]);
			return $response->withJson(['status'=> 'failed', 'message'=> 'You are not authorized to use this API.'], 400);
		}

		// standard middelware
		$response = $next($request, $response);
		return $response;
	}
}