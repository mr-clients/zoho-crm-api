<?php 

namespace App\Controllers;

use \Slim\Views\Twig as View; 

class HomeController extends Controller
{

    public function index($request, $response) 
    {
        $this->container->view->render($response, 'home.twig');
    }
}