<?php 

namespace App\Controllers;

use App\Models\Zoho;
use Respect\Validation\Validator as v;

class ApiController extends Controller
{

    public function addToken($request, $response) 
    {

        $validation = $this->container->validator->validate( $request, [ 
            'token' => v::stringType()->length(1, 100)->notEmpty(),
        ]);

        if ( $validation->failed() ) {
            $errors = $_SESSION['errors'];
            unset($_SESSION['errors']);
            return $response->withJson(['status'=> 'failed', 'message'=> $errors], 400);
        }

        $zoho = new Zoho();
        $zoho->addToken($request);

        return $response->withJson(['status'=> 'success', 'message'=> 'Token was created successfully.'], 200);
    }

    public function getSelfClient($request, $response)
    {
        $this->container->view->render($response, 'api/zoho/self-client.twig');
    }

    public function postSelfClient($request, $response) 
    {

        $validation = $this->container->validator->validate( $request, [ 
            'token' => v::stringType()->length(1, 100)->notEmpty(),
        ]);

        if ( $validation->failed() ) {
            return $response->withRedirect($this->router->pathFor('api.zoho.self-client'));
        }

        $zoho = new Zoho();
        if ($zoho->addToken($request)) {
            $this->container->flash->addMessage('global', 'Token successfully generated.');
            return $response->withRedirect($this->router->pathFor('home'));
        } else {
            $this->container->flash->addMessage('error', 'Error creating new token.');
            return $response->withRedirect($this->router->pathFor('api.zoho.self-client'));
        }
    }

    public function getZohoCredentials($request, $response, $args)
    {
        $this->container->view->render($response, 'api/zoho/setup-credentials.twig');
    }

    public function postZohoCredentials($request, $response, $args)
    {

        $validation = $this->container->validator->validate( $request, [ 
            'client_id' => v::stringType()->length(1, 100)->notEmpty(),
            'client_secret' => v::stringType()->length(1, 100)->notEmpty(),
            'redirect_uri' => v::url(),
            'current_user_email' => v::email(),
        ]);

        if ( $validation->failed() ) {
            return $response->withRedirect($this->router->pathFor('api.zoho.setup-credentials'));
        }

        $zoho = new Zoho();
        if ($zoho->saveCredentials($request)) {
            $this->container->flash->addMessage('global', 'Credentials saved correctly.');
            return $response->withRedirect($this->router->pathFor('home'));
        } else {
            $this->container->flash->addMessage('error', 'Error saving your credentials.');
            return $response->withRedirect($this->router->pathFor('api.zoho.setup-credentials'));
        }
    }

    public function getLogging($request, $response)
    {
        $logs = [];
        if (file_exists($this->container['settings']['logfile'])) {
            $logfile = fopen($this->container['settings']['logfile'],"r");
        
            while(! feof($logfile)) {
                $logs[] = json_decode(fgets($logfile));
            }
    
            fclose($logfile);
            $this->container->view->getEnvironment()->addGlobal('logs', $logs);
            $this->container->view->render($response, 'api/logs.twig');
        } else {
            $this->container->flash->addMessage('dark', "There currently aren't any logs to analyze.");
            return $response->withRedirect($this->router->pathFor('home'));

        }

    }
}