<?php 

namespace App\Controllers;

use App\Models\User;
use App\Models\Accounts;
use Respect\Validation\Validator as v;

class AccountController extends Controller
{
    
    public function getAccounts($request, $response) 
    {
        $Accounts = new Accounts();
        $accounts = $Accounts->getAccounts();

        $this->container->view->getEnvironment()->addGlobal('accounts', $accounts);

        $this->container->view->render($response, 'accounts.twig');

    }

    
    public function getAccount($request, $response, $args) 
    {
        $Accounts = new Accounts();
        $account = $Accounts->getAccount($args['id']);

        $this->container->view->getEnvironment()->addGlobal('account', $account);

        $this->container->view->render($response, 'account/edit.twig');

    }
    
    public function postAccount($request, $response, $args) 
    {
        $User = new User($request->getParam('id'));
        
        if ($User->exists()) {
            // Validate fields
            $validation = $this->container->validator->validate( $request, [ 
                'fullname' => v::stringType()->length(1, 50)->notEmpty(),
                'state' => v::stringType()->length(1, 10)->notEmpty(),
                'email' => v::email()->notEmpty(),
                'access' => v::arrayType(), 
                ]);
                
                // if validation fails return error
                if ( $validation->failed() ) {
                    return $response->withRedirect($this->router->pathFor('accounts.edit') . '/' . $request->getParam('id'));
                }
            }
            
        $update_response = $User->updateAccount($request);

        if ( $update_response === false ) {
            $this->container->flash->addMessage('error', 'Account update failed.');
            return $response->withRedirect($this->router->pathFor('accounts.edit') . '/' . $request->getParam('id'));
        } 

        $this->container->flash->addMessage('global', 'Account successfully updated.');
        return $response->withRedirect($this->router->pathFor('accounts'));
    }

    public function getCreate($request, $response)
    {
        $this->container->view->getEnvironment()->addGlobal('randId', rand(10000,99999));
        $this->container->view->getEnvironment()->addGlobal('randToken', bin2hex(openssl_random_pseudo_bytes(12)));
        $this->container->view->render($response, 'account/create.twig');
    }

    public function postCreate($request, $response)
    {
        // Validate fields
        $validation = $this->container->validator->validate( $request, [ 
            'id' => v::stringType()->length(1, 25),
            'fullname' => v::stringType()->length(1, 25),
            'email' => v::email(),
            'password' => v::alnum()->length(8, null),
            'token' => v::alnum()->length(8, null),
            'access' => v::arrayType(), 
        ]);
        
        // if validation fails return error
        if ( $validation->failed() ) {
            return $response->withRedirect($this->router->pathFor('accounts.create'));
        }

        $User = new User($request->getParam('id'));
        
        if ( $User->updateAccount($request, true) ) {
            $this->container->flash->addMessage('globals', 'Account successfully updated.');
            $this->container->view->getEnvironment()->addGlobal('id', $request->getParam('id'));
            $this->container->view->getEnvironment()->addGlobal('token', $request->getParam('token'));
            $this->container->view->getEnvironment()->addGlobal('domain', $_SERVER['DOMAIN']);
            return $this->container->view->render($response, 'auth/token/view.twig');
        }

        $this->container->flash->addMessage('errors', 'There was an error creating the user.');
        return $response->withRedirect($this->router->pathFor('accounts.create'));
    }
}