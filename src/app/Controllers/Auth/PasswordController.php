<?php 

namespace App\Controllers\Auth;

use App\Models\User;
use App\Controllers\Controller;
use Respect\Validation\Validator as v;

class PasswordController extends Controller 
{

    public function getChangePassword($request, $response, $args)
    {
        $this->container->view->getEnvironment()->addGlobal('id', $args['id']);
        return $this->container->view->render($response, 'auth/password/change.twig');
    }

    public function postChangePassword($request, $response)
    {
        $user = new User($request->getParam('id'));
        $validation = $this->container->validator->validate($request, [
            'password' => v::alnum()->length(8, null)->notEmpty(),
        ]);

        if ( $validation->failed()) {
            return $response->withRedirect($this->router->pathFor('auth.password.change') . '/' . $request->getParam('id'));
        }
        // change the password
        if ( ! $user->setPassword($request->getParam('password')) ) {
            $this->container->flash->addMessage('error', 'An error occured saving your password.');
            return $response->withRedirect($this->router->pathFor('auth.password.change') . '/' . $request->getParam('id'));
        }
        
        $this->container->flash->addMessage('global', 'Password was successfully changed.');
        return $response->withRedirect($this->router->pathFor('home'));
    }

}