<?php 

namespace App\Controllers\Auth;

use App\Models\User;
use App\Controllers\Controller;

class TokenController extends Controller 
{

    public function getToken($request, $response, $args)
    {
        foreach (['token', 'id'] as $key) {
            $this->container->view->getEnvironment()->addGlobal($key, $_SESSION[$key]);
            unset($_SESSION[$key]);
        }
        $this->container->view->getEnvironment()->addGlobal('domain', $_SERVER['DOMAIN']);
        return $this->container->view->render($response, 'auth/token/view.twig');
    }

    public function postTokenRegenerate($request, $response, $args)
    {
        $user = new User($args['id']);
        $token = $user->generateToken();

        // change the password
        if ( $token  === false ) {
            $this->container->flash->addMessage('error', 'An error occured generating a new token.');
            return $response->withRedirect($this->router->pathFor('accounts.edit') . '/' . $args['id']);
        }

        $_SESSION['token'] = $token;
        $_SESSION['id'] = $args['id'];
        $this->container->flash->addMessage('global', 'Token successfully generated.');
        return $response->withRedirect($this->router->pathFor('api.zoho.view'));
    }

}