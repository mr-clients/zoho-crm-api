<?php 

namespace App\Controllers\Auth;

use App\Controllers\Controller;
use Respect\Validation\Validator as v;

class AuthController extends Controller 
{

    public function postLogin($request, $response) 
    {
        $attempt = $this->container->auth->attempt($request->getParam('username'), $request->getParam('password'));

        if ($attempt) {
            return $response->withRedirect($this->router->pathfor('home'));
        }

        $this->container->flash->addMessage('error', 'Your username or password was incorrect.');

        return $response->withRedirect($this->router->pathfor('auth.login'));
    }

    public function getLogin($request, $response)
    {
        return $this->container->view->render($response, 'auth/login.twig');
    }

    public function logout($request, $response)
    {
        $this->container->auth->logout();

        return $response->withRedirect($this->router->pathfor('auth.login'));
    }
}