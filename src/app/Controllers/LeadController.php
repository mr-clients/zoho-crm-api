<?php 

namespace App\Controllers;

use App\Models\Zoho;
use Respect\Validation\Validator as v;
use zcrmsdk\oauth\exception\ZohoOAuthException;

class LeadController extends Controller
{

    /**
     * Pulls leads from CRM
     * !!Only used for testing not a production method
     *
     * @param   Object  $request   Slim request object
     * @param   Object  $response  Slim reponse object
     *
     * @return  Object             Slim reponse object
     */
    public function getLeads($request, $response)
    {
        // instantiate Zoho class
        $Zoho = new Zoho();
        // get leads from Zoho CRM API
        $message = $Zoho->getLeads();
        // return JSON object with leads
        return $response->withJson(['status'=> 'success', 'leads'=> $message], 200);
    }

    /**
     * Pushes single lead to CRM
     *
     * @param   Object  $request   Slim request object
     * @param   Object  $response  Slim response object
     *
     * @return  Object             Slim response object
     */
    public function addLead($request, $response) 
    {
        // create hash fingerprint for request
        $hash = hash('md5', implode(',', $request->getParams()));

        // log request along with hash
        $this->container->logger->info('Request', ['hash' => $hash, 'request', $request->getParams()]);

        // Validate fields
        $validation = $this->container->validator->validate( $request, [ 
            'firstName' => v::stringType()->length(1, 25)->notEmpty(), // First_Name
            'lastName' => v::stringType()->length(1, 25)->notEmpty(), // Last_Name
            'address' => v::stringType()->length(1,150)->notEmpty(), // Street
            'city' => v::alpha()->length(1, 25)->notEmpty(), // City
            'stateProvince' => v::alpha()->length(1, 25)->notEmpty(), // State
            'postalCode' => v::postalCode('US')->notEmpty(), // Zip_Code
            'primaryPhone' => v::optional(v::phone()), // Phone
            'phoneExt' => v::optional(v::stringType()->length(1, 10)), // Ext
            'secondaryPhone' => v::optional(v::phone()), // Mobile
            'secondaryPhoneExt' => v::optional(v::stringType()->length(1, 10)), // Ext_2
            'email' => v::optional(v::email()), // Email
            'fee' => v::optional(v::alnum('$.')), // Cost_per_Click
            'appointment' => v::optional(v::arrayType()), // All_Form_Info
            'interview' => v::optional(v::arrayType()), // All_Form_Info
            'taskName' => v::optional(v::stringType()->length(1, 255)), // All_Form_Info
            'comments' => v::optional(v::stringType()->length(1,255)), // Lead_Notes
        ]);
        
        // if validation fails return error
        if ( $validation->failed() ) {
            $errors = $_SESSION['errors'];
            unset($_SESSION['errors']);
            $this->container->logger->error('Validation.failed', ['hash' => $hash, 'response' => $api_reponse]);
            return $response->withJson(['status'=> 'failed', 'message'=> $errors], 400);
        }

        // bypass CRM API call if sandbox=true
        if ( $request->getParam('sandbox') == 'true' ) {
            // create sandbox response
            $api_reponse = [
                "status" => "success",
                "message" => "Sandbox has validated the fields successfully."
            ];
            // log sandbox response
            $this->container->logger->info('Sandbox.response', ['hash' => $hash, 'response' => $api_reponse]);
        } else {
            try {
                // instantiate Zoho class
                $Zoho = new Zoho();
                // call add lead which communicates with ZOHO API and adds lead to the CRM
                $api_reponse = $Zoho->addLead($request);
                // log API reponse
                $this->container->logger->info('Zoho.response', ['hash' => $hash, 'response' => $api_reponse]);
            } catch (ZohoOAuthException $e){
                // create resonse array from exception
                $api_reponse['status'] = 'failed';
                // only return generic error if displayErrorDetails is false
                $api_reponse['message'] = ($this->settings['displayErrorDetails'] == "true") ? $e->getMessage() : 'ZohoOAuthException happened please contact administrator.';
                // log exception
                $this->container->logger->error('OAuthException.addLead', [$e->getMessage()]);
            }
        }

        // return JSON response with array from CRM API
        return $response->withJson( ['status'=> $api_reponse['status'], 'message'=> $api_reponse], ($api_reponse['status'] == 'success') ? 200 : 400 );
    }
}