# Zoho JSON CRM endpoint
Written with PHP [Slim Framework](https://www.slimframework.com/docs/v4/).  
This endpoint accepts JSON requests from lead generation companies. It includes a simple panel that allows you to add new API consumers.

## Panel
Default login credentials are admin:password. You should change after deploying.

## Docker
### Build
#### Requirements
* Docker
* Docker Compose

##### Example Docker Compose File
```yaml
version: "3"
services:
  app:
    image: marvinroman/zohocrm-api:latest
    build: ./
    ports:
      - "80:80"
    environment:
      - "DOMAIN=localhost"
      - "PUID=501" # set to the numeric ID if the current user on your computer find using ls -ln
      - "REMOVE_FILES=0" # don't remove files in the /var/www/html directory
      - "SKIP_CHOWN=1" # skip changing ownership of files
      - "WEBROOT=/var/www/html/public"
      - "ERRORS=1" # Show PHP errors
      - "CLIENT_ID=1000.234ASDF3QRFASDF89AJSDFFASDF" # ZOHO API client_id
      - "CLIENT_SECRET=fasdf23rs8dvzdvjql3rqwef9z8dfhdjf" # ZOHO API client_secret
      - "REDIRECT_URI=http://domain.com/oathcallback" # ZOHO API redirect uri
      - "CURRENT_USER_EMAIL=support@domain.com" # ZOHO API mail
      - "SANDBOX=false" # If you have a ZOHO sandbox account whether to use it for testing
      - "DISPLAY_ERROR_DETAILS=true" # show slim errors
      - "SKIP_COMPOSER=1" # don't install composer on startup
    volumes:
      - "./src:/var/www/html:cached"
```  

**Clone repository**  
```bash 
git clone https://gitlab.com/mr-clients/zoho-crm-api.git
```  

**Enter directory**  
```bash
cd zoho-crm-api
```  

**Build using Docker Compose**  
```bash
docker-compose build app
```
**OR**  
**Build regularly**  
```bash
docker build -t marvinroman/zohozrm-api:latest .
```  

### Deploy
#### Docker Compose
```bash
docker-compose up -d
```

#### Run
```bash
docker run -d -v ./src:/var/www/html -p 80:80 \
-e DOMAIN=localhost \
-e PUID=501 \
-e REMOVE_FILES=0 \
-e SKIP_CHOWN=1 \
-e WEBROOT=/var/www/html/public \
-e ERRORS=1 \
-e CLIENT_ID=1000.234ASDF3QRFASDF89AJSDFFASDF \
-e CLIENT_SECRET=fasdf23rs8dvzdvjql3rqwef9z8dfhdjf \
-e REDIRECT_URI=http://domain.com/oathcallback \
-e CURRENT_USER_EMAIL=support@domain.com \
-e SANDBOX=false \
-e DISPLAY_ERROR_DETAILS=true \
-e SKIP_COMPOSER=1 \
marvinroman/zohozrm-api:latest
```