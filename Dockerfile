FROM richarvey/nginx-php-fpm

# Install SASS compiler
RUN apk add --no-cache npm
RUN npm install -g sass 

# Install YAML PHP extension
RUN apk add --virtual .permanent-dependancies \
    yaml
RUN apk add --virtual .build-dependancies \
    autoconf \
    g++ \
    make \
    yaml-dev
RUN pecl channel-update pecl.php.net
RUN pecl install yaml && docker-php-ext-enable yaml
RUN apk del .build-dependancies

COPY ./etc /etc 

COPY ./usr /usr
RUN chmod +x /usr/bin/*

COPY ./src /var/www/html

COPY ./scripts/start.sh /start.sh 
RUN chmod +x /start.sh